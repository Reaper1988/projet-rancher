<!DOCTYPE html>
<html>
    <head>
        <title>Projet Rancher</title>
        <style>
            section {
                background-color: #333;
                width: 80%;
                margin: 0 auto;
                padding: 10px;
                height: 100%;
            }
            h1 {
                background-color: #fff;
                border-radius: 50%;
                color: #000;
                margin: auto 0;
                line-height: 50px;
                text-align: center;
            }
            p {
                width: 100%;
                text-align: center;
                color:#fff;
            }
        </style>
    </head>
    <body>
        <section>
            <div>
                <h1>Projet Rancher - NCRC1162</h1>
                <p>
                    Host: <?php echo $_SERVER['HTTP_HOST']; ?><br />
                    IP: <?php echo $_SERVER['SERVER_ADDR']; ?><br />
                    Port: <?php echo $_SERVER['SERVER_PORT']; ?><br />
                </p>
            </div>
        </section>
    </body>
</html>